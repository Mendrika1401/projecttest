/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;
import anno.UrlAnnotation;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import tools.ModelView;

/**
 *
 * @author Sasuke-DESKTOP
 */
public class Personne {
   String nom;
   String prenom;
   Date datenaissance;
   Double taille;

    public Personne() {
    }

   
    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
      
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public Double getTaille() {
        return taille;
    }

    public void setTaille(Double taille) {
        this.taille = taille;
    }
    
   @UrlAnnotation(urlname = "pers.do")
    public ModelView getpersonne() {
        HashMap <String, Object>data=new HashMap<String, Object>();
        data.put("pers", this);
        ModelView iray=new ModelView( "pers.jsp" , data);
        return iray;
    }
    
    @UrlAnnotation(urlname = "Lspers.do")
    public ModelView listpersonne() {
        HashMap <String, Object>data=new HashMap<String, Object>();
        ArrayList<Personne> lp=new ArrayList<Personne>();
            lp.add(new Personne("Rakoto", "Hery"));
            lp.add(new Personne("Ramaro", "Bema"));
            lp.add(new Personne("Rabe", "nary"));
        data.put("Listp",lp);
        ModelView iray=new ModelView( "Listpers.jsp" , data);
        return iray;
    }
}
